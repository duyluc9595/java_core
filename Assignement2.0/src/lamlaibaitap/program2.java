package lamlaibaitap;

import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class program2 {
    public static void main(String[] args) {
//        System.out.println("khởi tạo một giá trị đối tượng");

//       ----- Khỏi tạo giá trị cho đối tượng Deprtment-----
//        đối tượng 1 :
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "sale";

//        đối tượng 2 :
        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "marketing";
//        đối tượng 3 :
        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "tài chính";
//        ----- Khỏi tạo giá trị cho đối tượng  Position-----
//        đối tượng 1 :
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.SCRUM_MASTER;

//        đối tượng 2 :
        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.DEV;
//        đối tượng 3 :
        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.TEST;
//        ----- Khỏi tạo giá trị cho đối tượng  Account-----
//         đối tượng 1 :
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "NguyenVanA@gmail.com";
        account1.username = "use 1";
        account1.fullName = "Nguyễn Văn A";
        account1.department = department1;
        account1.position = position1;


//          đối tượng 2 :
        Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "NguyenVanB@gmail.com";
        account2.username = "use 2";
        account2.fullName = "Nguyễn Văn B";
        account2.department = department3;

        ;
//          đối tượng 3 :
        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "email13@gmail.com";
        account3.username = "use 3";
        account3.fullName = "fullname 3";
        account3.department = department2;

//          đối tượng 4 :
        Account account4 = new Account();
        account4.accountId = 4;
        account4.email = "email13@gmail.com";
        account4.username = "use 4";
        account4.fullName = "fullname 4";
        account4.department = department3;

//        ----- Khỏi tạo giá trị cho đối tượng  Group-----
//        đối tượng 1 :
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "grop1";
        group1.creatorId = account1;
        group1.createDate = new Date();

//        đối tượng 2 :
        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "grop2";
        group2.creatorId = account2;
        group2.createDate = new Date();

//        đối tượng 3 :
        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "grop3";
        group3.creatorId = account4;
        group3.createDate = new Date();
//        ----- Khỏi tạo giá trị cho đối tượng  GroupAccount-----
//        đối tượng 1 :
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.accountId = account1;
        groupAccount1.group = group1;
        groupAccount1.joinDate = new Date();

//        đối tượng 2 :
        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.accountId = account2;
        groupAccount2.group = group2;
        groupAccount2.joinDate = new Date();
//        đối tượng 3 :
        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.accountId = account4;
        groupAccount3.group = group3;
        groupAccount3.joinDate = new Date();
//        ----- Khởi tạo giá trị cho đối tượng  TypeQuestion-----
//        đối tượng 1 :
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = TypeName.ESSAY;

//        đối tượng 2 :
        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName = TypeName.ESSAY;
//        đối tượng 3 :
        TypeQuestion typeQuestion3 = new TypeQuestion();
        typeQuestion3.typeId = 3;
        typeQuestion3.typeName = TypeName.MULTIPLE_CHOICE;
//        ----- Khỏi tạo giá trị cho đối tượng  CategoryQuestion-----
//        đối tượng 1 :
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = "java";

//        đối tượng 2 :
        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryId = 2;
        categoryQuestion2.categoryName = "SQL";
//        đối tượng 3 :
        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryId = 3;
        categoryQuestion3.categoryName = "Ruby";
//        ----- Khỏi tạo giá trị cho đối tượng  Question-----
//        đối tượng 1 :
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "cau hoi 1";
        question1.category = categoryQuestion1;
        question1.typeId = typeQuestion1;
        question1.creatorId = account1;
        question1.createDate = new Date();

//        đối tượng 2 :
        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "cau hoi 2";
        question2.category = categoryQuestion2;
        question2.typeId = typeQuestion2;
        question2.creatorId = account2;
        question2.createDate = new Date();
//        đối tượng 3 :
        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "cau hoi 3";
        question3.category = categoryQuestion3;
        question3.typeId = typeQuestion3;
        question3.creatorId = account4;
        question3.createDate = new Date();
//        ----- Khỏi tạo giá trị cho đối tượng  Answer-----
//        đối tượng 1 :
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.content = "cau tra loi 1";
        answer1.question = question1;
        answer1.isCorrect = true;

//        đối tượng 2 :
        Answer answer2 = new Answer();
        answer2.answerId = 2;
        answer2.content = "cau tra loi 2";
        answer2.question = question2;
        answer2.isCorrect = true;
//        đối tượng 3 :
        Answer answer3 = new Answer();
        answer3.answerId = 3;
        answer3.content = "cau tra loi 3";
        answer3.question = question3;
        answer3.isCorrect = false;
        //        ----- Khỏi tạo giá trị cho đối tượng  Exam-----
//        đối tượng 1 :
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "code01";
        exam1.tile = "de thi 1";
        exam1.creatorId = account1;
        exam1.duration = 90;
        exam1.createDate = new Date();

//        đối tượng 2 :
        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = "code02";
        exam2.tile = "de thi 2";
        exam2.creatorId = account2;
        exam2.duration = 60;
        exam2.createDate = new Date();
//        đối tượng 3 :
        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = "code03";
        exam3.tile = "de thi 3";
        exam3.creatorId = account4;
        exam3.duration = 30;
        exam3.createDate = new Date();

//        ----- Khỏi tạo giá trị cho đối tượng  ExamQuestion---
//        đối tượng 1 :
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.exam = exam1;
        examQuestion1.question = question1;
//        đối tượng 2 :
        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.exam = exam2;
        examQuestion2.question = question2;
//        đối tượng 3 :
        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.exam = exam3;
        examQuestion3.question = question3;

//        Question 1:
//        tạo ra một array(mảng) để kiểm tra và lấy ra các phần tử ta cần
        Account[] accounts = {account1, account2, account3, account4};
        Group[] groups = {group1, group2, group3};
        group1.accounts = new Account[]{account1, account2};
//        ta tạo thêm dữ liệu vào các account chưa có group để lấy ra các phần tử trong nó
        account2.groups = new Group[]{group1, group2};
//      if  thường :
//        if (account2.department == null) {
//            System.out.println("nhan vien nay chua co phong ban");
//        }else {
//                System.out.println("phong ban cua nhan vien nay la" + account2.department.departmentName);
//            }

//         toán tử ba mgôi :
//       String dpt = account2.department == null ? ("nhan vien nay chua co phong ban ") : ("nhan vien nay la " + account2.department.departmentName);
//        System.out.println(dpt);

//        Question 2:
//        if (account2.groups == null){
//            System.out.println("nhan vien nay chua co group");
//        }else if (account2.groups.length <= 2 ){
//            System.out.println("group cua nhan vien nay laf 1-2");
//        }else if (account2.groups.length <=3){
//            System.out.println("nhan vien nay la nguoi quan trong");
//        }else {
//            System.out.println("nhan vien nay la nguoi hong chuyen");
//        }
//        Question 3:
//        String positionaccount1 = account1.position.positionName == PositionName.DEV ? "Đây là Developer" : "Người này không phải là Developer" ;
//        System.out.println(positionaccount1);

//        Question 5:
//        thêm phần tử account vào trong nhóm thì ta mới thao tác đếm và lấy ra dữ liệu cần lấy.
        account2.groups = new Group[]{group1};
//        switch (group1.accounts.length) {
//            case 1:
//                System.out.println("nhom co 1 thanh vien");
//                break;
//            case 2:
//                System.out.println("nhom co hai thanh vien");
//                break;
//            case 3:
//                System.out.println(" nhom co ba thanh vien ");
//                break;
//            default:
//                System.out.println(" nhom co nhieu thanh vien");
//        }

//        int grouplength = account2.groups.length;
//
//        if (grouplength == 0) {
//            System.out.println("nhan vien nay chua co group");
//        }
//        switch (grouplength) {
//            case 1:
//                System.out.println("Group của nhân viênnày là Java Fresher, C# Fresher");
//                break;
//            case 2:
//                System.out.println("Group của nhân viênnày là Java Fresher, C# Fresherc#");
//                break;
//            case 3:
//                System.out.println("nhan vien nay la nguoi quan trong");
//                break;
//            case 4:
//                System.out.println("nhan v ien nay la nguoi hong chuyen");
//                break;
//        }
//        Question 7:
//        toString : sử dụng khi đối tượng cần hiển thị dưới dạng văn bản hay gọi là chuỗi
//        switch (account1.position.positionName.toString()) {
//            case "DEV":
//                System.out.println("Đây là Developer");
//            default:
//                System.out.println("Người này không phải là Developer " + " mà là " + account1.position.positionName);
//        }
//        Question 8:
//        for ( Account account : accounts) {
//            System.out.println("email la : " + account.email);
//            System.out.println("fullname la : " + account.fullName);
//            System.out.println("phong ban la : " + account.department.departmentName);
//        }
//        Question 9:
        Department[] d = {department1, department2, department3};
//        for (Department department : d) {
//            System.out.println("id phong ban :" + department.departmentId);
//            System.out.println("ten phong ban :"+ department.departmentName);
//        }
////        Question 10:
//        for (int i = 0; i < accounts.length;i++){
//            System.out.println("....................");
//            System.out.println("thong tin account thu " + i);
//            System.out.println("emaill :" + accounts[i].email);
//            System.out.println("fullname :" + accounts[i].fullName);
//            System.out.println("phong ban :" + accounts[i].department.departmentName);
//        }
//        Question 11:
//        for (int i = 0; i < d.length;i++){
//            System.out.println("....................");
//            System.out.println("thong tin department thu " + i);
//            System.out.println("emaill :" + d[i].departmentId);
//            System.out.println("fullname :" + d[i].departmentName);
//        }

//        Question 12:
//        for (int i = 0; i < d.length;i++) {
//            if (i < 2) {
//                System.out.println("....................");
//                System.out.println("thong tin department thu " + i);
//                System.out.println("emaill :" + d[i].departmentId);
//                System.out.println("fullname :" + d[i].departmentName);
//            }
//        }
////        Question 13:
//        for (int i = 0; i < accounts.length;i++) {
//            if (i != 2  ) {
//                System.out.println("....................");
//                System.out.println("thong tin account thu " + i);
//                System.out.println("emaill :" + accounts[i].email);
//                System.out.println("fullname :" + accounts[i].fullName);
//                System.out.println("phong ban :" + accounts[i].department.departmentName);
//            }
//        }
//        Question 14:
//        for (int i = 0; i < accounts.length;i++) {
//            if (i < 4) {
//                System.out.println("....................");
//                System.out.println("thong tin account thu " + i);
//                System.out.println("emaill :" + accounts[i].email);
//                System.out.println("fullname :" + accounts[i].fullName);
//                System.out.println("phong ban :" + accounts[i].department.departmentName);
//            }
//        }

//        Question 15:
//        for (int i=0;i<=20 ; i++){
//            if (i%2==0)
//            System.out.println(i);
//        }
//        Question 16-1:
//        int i=0;
//        while (i<accounts.length){
//            System.out.println("........................");
//            System.out.println("email :"+ accounts[i].email);
//            System.out.println("fullname: "+ accounts[i].fullName);
//            System.out.println("phong ban: "+ accounts[i].department.departmentName);
//            i++;
//        }
//        Question 16-2:
        int i = 0;
//        while (i<d.length){
//            System.out.println("........................");
//            System.out.println("id phong ban:" + d[i].departmentId);
//            System.out.println("name phong ban :"+ d[i].departmentName);
//            i++;
//        }
//        Question 16-3:
//        while (i<accounts.length){
//            System.out.println("....................");
//            System.out.println("thong tin account thu " + i);
//            System.out.println("emaill :" + accounts[i].email);
//            System.out.println("fullname :" + accounts[i].fullName);
//            System.out.println("phong ban :" + accounts[i].department.departmentName);
//        i++;
//        }
//        Question 16-4:
//        while (i<d.length) {
//            System.out.println("........................");
//            System.out.println("thong tin department thu " + i);
//            System.out.println("id phong ban:" + d[i].departmentId);
//            System.out.println("name phong ban :" + d[i].departmentName);
//            i++;
//        }
//        Question 16-5:
//        while (i<accounts.length){
//            if (i<2) {
//                System.out.println("........................");
//                System.out.println("email :" + accounts[i].email);
//                System.out.println("fullname: " + accounts[i].fullName);
//                System.out.println("phong ban: " + accounts[i].department.departmentName);
//            }
//            i++;
//        }
//        Question 17:
//        do {
//            System.out.println("........................");
//            System.out.println("thong tin acount: "+ (i+1)+ " la");
//            System.out.println("email :" + accounts[i].email);
//            System.out.println("fullname: " + accounts[i].fullName);
//            System.out.println("phong ban: " + accounts[i].department.departmentName);
//            i++;
//        }while (i<accounts.length);
//        Exercise 2
//        Question 1:
//        int x = 5;
//        System.out.println(x);
//        Question 2:
//int x = 100000000;
//        System.out.println(x);
//        Question 3:
//        double x = 5.567098;
//        System.out.println(x);
//
//        int i1 = 100000000;
//        System.out.println(Locale.us ,"%,d %n , 100000000);
//
            Scanner scanner = new Scanner(System.in);
            Account account = new Account();
        Position position5 = new Position();
        position5.positionName = PositionName.TEST;
        Position p = new Position();
        p.positionName = PositionName.TEST;
//            position1.positionName = position1.positionName.
                    System.out.println("mời bạn tạo account");
            int input = scanner.nextInt();
            switch (input) {
                case 1:
                    account.position = position5;
                    break;
                case 2:
                    account.position = p;
                    break;
        }
    }
}

