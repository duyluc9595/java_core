package com.vti.testingsystem89.frontend;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class DeemSet {
    public static void main(String[] args) {
        DemoHashSet();
    }

    public static void DemoHashSet(){
//         set ko lưu 2 phần tử giống nhau
//        set tự xắp xếp giá trị mình tuyền vào từ bé tới lớn
        Set<Integer> integerSet = new HashSet<>();
        integerSet.add(1); // index 0
        integerSet.add(2);  // index 1
        integerSet.add(4);
        integerSet.add(3);
        integerSet.add(5);
        integerSet.add(1);
        System.out.println(integerSet);

        //         kiểm tra số phần tử :
        System.out.println("số phần tử trong list " + integerSet.size());

//         không có hàm get(index) so với list
//        System.out.println("vị trí có index = 1 là :" + integerSet);

//         xáo phần tử (có 2 cách)
//        c1 : truyền index của phần tử muốn xóa (thì dữ liệu trả  về là đối tượng E mà mình làm việc)
        System.out.println("xóa :" + integerSet.remove(1));

        System.out.println(integerSet);

//        cách 2 : truyền vào đối tượng mà mình muốn xóa (đối tượng truyền vào phải giống E) (dữ liệu trả về là boolean)

        System.out.println("xóa :" + integerSet.remove(2));
        System.out.println(integerSet);

        Iterator<Integer> stringIterator = integerSet.iterator();
        while ((stringIterator.hasNext())){
            System.out.println(stringIterator.next());
        }
    }
    }

