package com.vti.testingsystem89.frontend;

import java.util.*;

public class DemoList {
    public static void main(String[] args) {
//        demoArrayList();
//        demoLinkList();
        demoSort();
    }


    public static void demoArrayList(){
        List<Integer> integerArrayList = new ArrayList<>();
        integerArrayList.add(1);
        integerArrayList.add(2);
        integerArrayList.add(3);
        integerArrayList.add(4);
        integerArrayList.add(5);
        integerArrayList.add(1);

//         kiểm tra số phần tử :
        System.out.println("số phần tử trong list " + integerArrayList.size());

//         lấy phần tử ở vị trí index thứ n (index bắt đầu từ 0)
        System.out.println("vị trí có index = 1 là :" + integerArrayList.get(1));

//         xáo phần tử (có 2 cách)
//        c1 : truyền index của phần tử muốn xóa (thì dữ liệu trả  về là đối tượng E mà mình làm việc)
//        System.out.println("xóa :" + integerArrayList.remove(1));
//
//        System.out.println(integerArrayList);

//        cách 2 : truyền vào đối tượng mà mình muốn xóa (đối tượng truyền vào phải giống E) (dữ liệu trả về là boolean)

        System.out.println("xóa :" + integerArrayList.remove(2));
        System.out.println(integerArrayList);

        Iterator<Integer> stringIterator = integerArrayList.iterator();
        while ((stringIterator.hasNext())){
            System.out.println(stringIterator.next());
        }
    }

    public static void demoLinkList(){
        List<Integer> integerArrayList = new ArrayList<>();
        integerArrayList.add(1); // index 0
        integerArrayList.add(2); // index 1
        integerArrayList.add(3);
        integerArrayList.add(4);
        integerArrayList.add(5);
        integerArrayList.add(1);

//         kiểm tra số phần tử :
        System.out.println("số phần tử trong list " + integerArrayList.size());

//         lấy phần tử ở vị trí index thứ n (index bắt đầu từ 0)
        System.out.println("vị trí có index = 1 là :" + integerArrayList.get(1));

//         xáo phần tử (có 2 cách)
//        c1 : truyền index của phần tử muốn xóa (thì dữ liệu trả  về là đối tượng E mà mình làm việc)
//        System.out.println("xóa :" + integerArrayList.remove(1));
//
//        System.out.println(integerArrayList);

//        cách 2 : truyền vào đối tượng mà mình muốn xóa (đối tượng truyền vào phải giống E) (dữ liệu trả về là boolean)

        System.out.println("xóa :" + integerArrayList.remove(2));
        System.out.println(integerArrayList);

        Iterator<Integer> stringIterator = integerArrayList.iterator();
        while ((stringIterator.hasNext())){
            System.out.println(stringIterator.next());
        }
    }

    public static void demoSort(){

        List<Integer> integerArrayList = new ArrayList<>();
        integerArrayList.add(1);
        integerArrayList.add(2);
        integerArrayList.add(3);
        integerArrayList.add(4);
        integerArrayList.add(5);
        integerArrayList.add(1);
        System.out.println(integerArrayList);
// xắp xếp theo thứ tự tăng dần
        Collections.reverse(integerArrayList);
        System.out.println(integerArrayList);

//        đảo ngược lại danh sách
        Collections.sort(integerArrayList);
        System.out.println(integerArrayList);


    }
    }

