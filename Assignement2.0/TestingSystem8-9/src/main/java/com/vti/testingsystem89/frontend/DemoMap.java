package com.vti.testingsystem89.frontend;

import java.util.HashMap;
import java.util.Map;

public class DemoMap {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();

        map.put("1","giá trị  1");
        map.put("2","giá trị  2");
        map.put("3","giá trị  3");
        map.put("4","giá trị  4");

        System.out.println(map);

//         lấy ra giá trị trong map
        System.out.println(map.get("3"));

//
        System.out.println(map.get(1));
    }

}
