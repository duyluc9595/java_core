module com.vti.testingsystem89 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.vti.testingsystem89 to javafx.fxml;
    exports com.vti.testingsystem89;
}