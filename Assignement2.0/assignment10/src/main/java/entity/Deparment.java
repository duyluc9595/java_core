package entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Deparment {
    private int departmentid;
    private String departmentName;
}
