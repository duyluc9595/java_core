package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class User {
    private int id;
    private String email;
    private String password;
    private String username;
    private String dateOfBirth;
    private Deparment deparment;
    private Role role;

}
