package frontend;

import Utils.ScannerUtils;
import backend.controller.UserController;
import entity.Deparment;
import entity.User;

import java.util.List;

public class Function {
    UserController userController = new UserController();

    public User login() {
        String username = ScannerUtils.inputString();
        String password = ScannerUtils.inputPassword();
        return userController.login(username, password);
    }

    public void getAllUsers() {
        List<User> userList = userController.getAllUser();
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user : userList) {
            System.out.format(leftAlignFormat, user.getId(),user.getRole(),user.getUsername(),user.getEmail(),
                    user.getDateOfBirth(),user.getDeparment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void finById(){
        System.out.println("mời bạn nhập vào Id cần tìm");
        int id = ScannerUtils.inputNumber();
        List<User> userList = userController.finByiD(id);
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user : userList) {
            System.out.format(leftAlignFormat, user.getId(),user.getRole(),user.getUsername(),user.getEmail(),
                    user.getDateOfBirth(),user.getDeparment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void FinbyuserId(){
        System.out.println("mời bạn nhập vào id user muốn tìm kiếm ");
        int id = ScannerUtils.inputNumber();
        User user = userController.FinbyuserId(id);
        if (user != null){
            String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
            System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
            System.out.format(leftAlignFormat, user.getId(),user.getRole(),user.getUsername(),user.getEmail(),
                    user.getDateOfBirth(),user.getDeparment().getDepartmentName());
        }else {
            System.out.println("userId không tồn tại mời bạn nhập lại");
        }
    }


//    chữa bài tập :
  public void finbyUser(){
      System.out.println("nhập từ khóa tìm kiếm :");
      String Key = ScannerUtils.inputString();
      List<User> userList = userController.finbyUser(Key);
      String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
      System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
      System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
      System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
      for (User user : userList) {
          System.out.format(leftAlignFormat, user.getId(),user.getRole(),user.getUsername(),user.getEmail(),
                  user.getDateOfBirth(),user.getDeparment().getDepartmentName());
      }
      System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
  }

    public void FinByUser(){
        System.out.println("mời bạn nhập vào Username hoặc email cần tìm");
        String username = ScannerUtils.inputString();
        String email = ScannerUtils.inputEmail();
        List<User> userList = userController.FinByUser(username,email);
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user : userList) {
            System.out.format(leftAlignFormat, user.getId(),user.getRole(),user.getUsername(),user.getEmail(),
                    user.getDateOfBirth(),user.getDeparment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }


    public void getAllDeparment() {
        List<Deparment> deparmentList = userController.getAllDeparment();
        String leftAlignFormat = "| %-3s| %-15s |%n";
        System.out.format("+----+-----------------+%n");
        System.out.format("| id | department_name |%n");
        System.out.format("+----+-----------------+%n");
        for (Deparment deparment : deparmentList) {
            System.out.format(leftAlignFormat, deparment.getDepartmentid(), deparment.getDepartmentName());
        }
        System.out.format("+----+-----------------+%n");
    }

    public void FinByDeparmentId(){
        System.out.println("Mời bạn nhập id Deparment muốn tìm");
        int id = ScannerUtils.inputNumber();
        List<Deparment> deparmentList = userController.FinByDeparmentId(id);
        String leftAlignFormat = "| %-3s| %-15s |%n";
        System.out.format("+----+-----------------+%n");
        System.out.format("| id | department_name |%n");
        System.out.format("+----+-----------------+%n");
        for (Deparment deparment : deparmentList) {
            System.out.format(leftAlignFormat, deparment.getDepartmentid(), deparment.getDepartmentName());
        }
        System.out.format("+----+-----------------+%n");
    }

// chữa bài tập
    public void deletebyUserId(){
        System.out.println("nhập id cần xóa");
        int id = ScannerUtils.inputNumber();
        User user = userController.FinbyuserId(id);
        if (user == null){
            System.out.println("userId không tồn tại");
        }else {
//            userController.deleteUser(id);
        }
    }
    public void updatePassword(){
        System.out.println("mòi bạn nhập vào id");
        int id = ScannerUtils.inputNumber();
        System.out.println("mòi bạn nhập vào password cũ");
        String OldPassword = ScannerUtils.inputPassword();
        System.out.println("mòi bạn nhập vào password mói");
        String newPassword = ScannerUtils.inputPassword();
        userController.updateUser(id,OldPassword,newPassword);
    }

    public void createUser (){
        System.out.println("mòi bạn nhập vào username ");
        String username = ScannerUtils.inputString();
        System.out.println("nhập vào email");
        String email = ScannerUtils.inputEmail();
        System.out.println("nhập vào ngày sinh");
        String birthday = ScannerUtils.inputString();

        System.out.println("mơi");
    }
}
