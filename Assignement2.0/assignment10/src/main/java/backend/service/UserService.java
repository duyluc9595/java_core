package backend.service;

import backend.repository.UserRepository;
import entity.Deparment;
import entity.User;

import java.util.List;

public class UserService implements IUserService{
    UserRepository userRepository = new UserRepository();

    @Override
    public User login(String username, String password) {
        return userRepository.login(username, password);
    }

    //    câu 1 :( Danh sách User);
    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUser();
    }

    //    câu 2 : Tìm kiếm User theo Id");
    @Override
    public List<User> finByiD(int id) {
        return userRepository.finByiD(id);
    }

    //    Câu 3 : Tìm kiếm User theo username và email");
    @Override
    public List<User> FinByUser(String username, String email) {
        return userRepository.FinByUser(username,email);
    }

    //    Câu 4 : Hiển thị danh sách tất cả các Deparment");
    @Override
    public List<Deparment> getAllDeparment() {
        return userRepository.getAllDeparment();
    }
    //    Câu 5 : Tìm kiếm Deparment theo Id");
    @Override
    public List<Deparment> FinByDeparmentId(int id) {
        return userRepository.FinByDeparmentId(id);
    }

    @Override
    public User FinbyuserId(int id) {
        return userRepository.FinbyuserId(id);
    }

    @Override
    public List<User> finbyUser(String key) {
        return userRepository.finbyUser(key);
    }


    @Override
    public void updateUser(int id, String oldpassword, String newpassword) {

    }

    @Override
    public void createUser(String username, String email, String birthDay, int departmentId) {

    }

//    Chức năng thứ 6 ADMIN:
    @Override
    public void deletebyuserId(int id) {
        userRepository.deleteUserById(id);
    }
}
