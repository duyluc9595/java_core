package backend.service;

import entity.Deparment;
import entity.User;

import java.util.List;

public interface IUserService {
// Đăng nhập login:
    User login(String username, String password);


//    câu 1 :( Danh sách User);
    List<User> getAllUser();

//    câu 2 : Tìm kiếm User theo Id");
    List<User> finByiD (int id);

//    Câu 3 : Tìm kiếm User theo username và email");
    List<User> FinByUser(String username, String email);

//    Câu 4 : Hiển thị danh sách tất cả các Deparment");
    List<Deparment> getAllDeparment();

//    Câu 5 : Tìm kiếm Deparment theo Id");
    List<Deparment> FinByDeparmentId(int id);

    User FinbyuserId (int id);

    List<User> finbyUser (String key);

//    Chức năng thứ 6 ADMIN:
    void deletebyuserId(int id);

    void updateUser (int id , String oldpassword, String newpassword);

    void createUser(String username, String email, String birthDay, int departmentId);
    }

