package backend.repository;

import Utils.JdbcUtil;
import entity.Deparment;
import entity.Role;
import entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

// câu 1:
public class UserRepository {
    public User login(String username, String password) {
        String sql = "SELECT * FROM user WHERE user_name = ? AND password = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setUsername(username);
                user.setEmail(resultSet.getString("email"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                return user;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JdbcUtil.closeConnection();
        }
        return null;
    }

//    tìm kiếm user theo Id:
    public List<User> getAllUser() {
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM `User` u JOIN Department D on u.department_id = D.department_id";
        Connection connection = JdbcUtil.GetConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
//                 lấy ra giá trị từng hàng gán vào đối tượng tương ứng
                User user = new User();
                Deparment deparment = new Deparment();
                int userId = resultSet.getInt("id");
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));

                deparment.setDepartmentName(resultSet.getString("department_name"));
                user.setDeparment(deparment);
                userList.add(user);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            JdbcUtil.closeConnection();
        }
        return userList;
    }

//    tìm kiếm user theo Id:
    public List<User> finByiD(int id) {
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM User JOIN Department D on User.department_id = D.department_id WHERE id = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                Deparment deparment = new Deparment();
                int userId = resultSet.getInt("id");
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                deparment.setDepartmentName(resultSet.getString("department_name"));
                user.setDeparment(deparment);
                userList.add(user);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return userList;
    }

//     Sửa bài trên lớp:
    public User FinbyuserId(int id) {
        User user = new User();
        String sql = "SELECT * FROM User U JOIN Department D on U.department_id = D.department_id WHERE id = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Deparment deparment = new Deparment();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                deparment.setDepartmentName(resultSet.getString("department_name"));
                user.setDeparment(deparment);
                return user;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            {
                JdbcUtil.closeConnection();
            }
             return user;
        }
    }

    public List<User> FinByUser(String username, String email) {
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM User WHERE user_name = ? OR email = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                Deparment deparment = new Deparment();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                deparment.setDepartmentName(resultSet.getString("department_name"));
                user.setDeparment(deparment);
                userList.add(user);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return userList;
    }

    public List<User> finbyUser(String key) {
        List<User> userList = new ArrayList<>();
        key = "%" + key + "%";
        String sql = "SELECT * FROM User U JOIN Department D on U.department_id = D.department_id WHERE U.user_name LIKE ? OR U.email LIKE ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, key);
            preparedStatement.setString(2, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                Deparment deparment = new Deparment();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                deparment.setDepartmentName(resultSet.getString("department_name"));
                user.setDeparment(deparment);
                userList.add(user);

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return userList;
    }
    // chức năng thứ 6 :
    public void deleteUserById(int id){
        String sql = "DELETE FROM User U WHERE U. id = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0 ){
                System.out.println("xóa không thành công");
            }else {
                System.out.println("đã xóa" + rs + "bản ghi");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }finally {
            JdbcUtil.closeConnection();
        }
    }
    public void  updateUser(int id , String oldpassword, String newpassword){
        String sql = "UPDATE User SET password = ? WHERE id = ? and password = ?";
        try {
            Connection connection = JdbcUtil.GetConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1 , newpassword);
            preparedStatement.setInt(2,id);
            preparedStatement.setString(3,oldpassword);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0){
                System.out.println("thay đỏi password thành công");
            }else {
                System.out.println("thay đổi psssword không thành công");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    public void createUser(String username, String email, String birthDay, int departmentId){
        String sql = "INSERT INTO assignment10.`User` (role, user_name, password, email, date_of_birth, department_id)\n" +
                "VALUES ('USER', ?, '123456', ?, ?, ?);";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, birthDay);
            preparedStatement.setInt(4, departmentId);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet == 0){
                System.out.println("Thêm mới không thành công");
            } else {
                System.out.println("Đã thêm mới " + resultSet + " user");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JdbcUtil.closeConnection();
        }
    }


// câu 4 - user:
    public List<Deparment> getAllDeparment() {
        List<Deparment> deparmentList = new ArrayList<>();
        String sql = "SELECT * FROM Department ";
        Connection connection = JdbcUtil.GetConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Deparment deparment = new Deparment();
                deparment.setDepartmentid(resultSet.getInt("department_id"));
                deparment.setDepartmentName(resultSet.getString("department_name"));
                deparmentList.add(deparment);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return deparmentList;
    }
// câu 5-user
    public List<Deparment> FinByDeparmentId(int department_id) {
        List<Deparment> deparmentList = new ArrayList<>();
        String sql = "SELECT * FROM Department WHERE department_id = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, department_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Deparment deparment = new Deparment();
                deparment.setDepartmentid(resultSet.getInt("department_id"));
                deparment.setDepartmentName(resultSet.getString("department_name"));
                deparmentList.add(deparment);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return deparmentList;
    }


}
