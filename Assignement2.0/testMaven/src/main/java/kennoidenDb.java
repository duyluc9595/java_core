import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class kennoidenDb {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        GetConnection();
    }
    static Connection connection = null;

    public static Connection GetConnection() {
        try {
            Properties properties = new Properties();
            properties.load( new FileInputStream("C:\\Users\\PC\\Documents\\java_core\\Assignement2.0\\testMaven\\src\\main\\resources\\db.properties"));
            String usename = properties.getProperty("user") ;
            String password = properties.getProperty("password");
            String url = properties.getProperty("url") ;
            String driver = properties.getProperty("driver");
            Class.forName(driver);
            connection = DriverManager.getConnection(url, usename, password);
            if (connection != null) {
                System.out.println("kết nối vào My SQL thành công");
            } else {
                System.out.println("kết nối thất bại");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }
}
