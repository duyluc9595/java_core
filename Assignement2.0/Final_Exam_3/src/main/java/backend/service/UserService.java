package backend.service;

import backend.repository.UserRepository;
import entity.User;

import java.util.List;

public class UserService implements IUserservice{
    UserRepository userRepository = new UserRepository();

//     câu 3 chức năng login :
    @Override
    public User login(String email, String password) {
        return userRepository.login(email, password);
    }

    @Override
    public User finbyProject(int id) {
        return userRepository.finbyProject(id);
    }

    @Override
    public User getAllManager(int managerid) {
        return userRepository.getallManager(managerid);
    }
}
