package backend.service;

import entity.User;

import java.awt.*;
import java.util.List;

public interface IUserservice {
     User login (String email, String password);

    User finbyProject(int id) ;

    User getAllManager (int managerid);
}
