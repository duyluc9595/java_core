package backend.controller;

import backend.service.UserService;
import entity.User;

import java.util.List;

public class UserController {
    UserService userService = new UserService();

//     câu 3 chức năng login :
    public User login(String email, String password) {
        return userService.login(email, password);
    }

    public User finbyProject(int id) {
        return userService.finbyProject(id);
    }

    public User getAllManager(int managerid) {
        return userService.getAllManager(managerid);
    }
}
