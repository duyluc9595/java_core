package backend.repository;

import Utils.JdbcUtil;
import entity.*;

import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    //     câu 3 chức năng login:
    public User login(String email, String password) {
        String sql = "SELECT * FROM `User` WHERE email LIKE ? AND password = ?";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, '%' + email + '%');
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setEmail(email);
                user.setFullName(resultSet.getString("full_name"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.closeConnection();
        }
        return null;
    }

//    câu 1 chức năng tìm kiếm project

    public User finbyProject(int id) {
        User user = new User();
        String sql = "select * from `User` join Manager m on u.Manager_Id = m.Manager_Id\n " +
                     "join Employee e on u.Employee_Id = e.Employee_Id\n " +
                     "where Project_id = ? ";
        Connection connection = JdbcUtil.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Manager manager = new Manager();
                Employee employee = new Employee();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setFullName(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                user.setProjectId(resultSet.getInt("ProjectId"));
                manager.setExpinYear(resultSet.getInt("expinYear"));
                String stringProskill = resultSet.getString("proskill");
              employee.setProskill(Proskill.valueOf(stringProskill));
              user.setEmployee(employee);
              user.setManager(manager);
               return user;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return user;
    }

//     lấy ra tất cả các manager của các project
public User getallManager(int managerid) {
    User user = new User();
    String sql = "select * from `User` join Manager m on u.Manager_Id = m.Manager_Id\n " +
            "join Employee e on u.Employee_Id = e.Employee_Id\n " +
            "where Manager_Id = ? ";
    Connection connection = JdbcUtil.GetConnection();
    try {
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1,managerid);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Manager manager = new Manager();
            Employee employee = new Employee();
            user.setId(resultSet.getInt("id"));
            String roleString = resultSet.getString("role");
            user.setRole(Role.valueOf(roleString));
            user.setFullName(resultSet.getString("full_name"));
            user.setEmail(resultSet.getString("email"));
            user.setProjectId(resultSet.getInt("ProjectId"));
            manager.setExpinYear(resultSet.getInt("expinYear"));
            String stringProskill = resultSet.getString("proskill");
            employee.setProskill(Proskill.valueOf(stringProskill));
            user.setEmployee(employee);
            user.setManager(manager);
            return user;
        }
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }
    return user;
}
}



