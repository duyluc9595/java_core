package entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee {
    private int employeeId;
    private Proskill proskill;

}
