package entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Manager {
    private int managerId;
    private int expinYear;
}
