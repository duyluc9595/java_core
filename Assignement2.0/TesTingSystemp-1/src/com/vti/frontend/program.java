package com.vti.frontend;

import java.util.Date;

public class program {
    public static void main(String[] args) {
        System.out.println("khởi tạo một giá trị đối tượng");

//       ----- Khỏi tạo giá trị cho đối tượng Deprtment-----
//        đối tượng 1 :
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "sale";

//        đối tượng 2 :
        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "marketing";
//        đối tượng 3 :
        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "tài chính";
//        ----- Khỏi tạo giá trị cho đối tượng  Position-----
//        đối tượng 1 :
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.SCRUM_MASTER;

//        đối tượng 2 :
        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.DEV;
//        đối tượng 3 :
        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.TEST;
//        ----- Khỏi tạo giá trị cho đối tượng  Account-----
//         đối tượng 1 :
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "NguyenVanA@gmail.com";
        account1.username = "use 1";
        account1.fullName = "Nguyễn Văn A";
        account1.department = department1;
        account1.position = position1;

//          đối tượng 2 :
       Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "NguyenVanB@gmail.com";
        account2.username = "use 2";
        account2.fullName = "Nguyễn Văn B";
        account2.department = department3;

        ;
//          đối tượng 3 :
        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "email13@gmail.com";
        account3.username = "use 3";
        account3.fullName = "fullname 3";
        account3.department = department2;

//          đối tượng 4 :
        Account account4 = new Account();
        account4.accountId = 4;
        account4.email = "email13@gmail.com";
        account4.username = "use 4";
        account4.fullName = "fullname 4";
        account4.department = department3;

//        ----- Khỏi tạo giá trị cho đối tượng  Group-----
//        đối tượng 1 :
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "grop1";
        group1.creatorId = account1;
        group1.createDate = new Date();

//        đối tượng 2 :
        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "grop2";
        group2.creatorId = account2;
        group2.createDate = new Date();

//        đối tượng 3 :
        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "grop3";
        group3.creatorId = account4;
        group3.createDate = new Date();
//        ----- Khỏi tạo giá trị cho đối tượng  GroupAccount-----
//        đối tượng 1 :
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.accountId = account1;
        groupAccount1.group = group1;
        groupAccount1.joinDate = new Date();

//        đối tượng 2 :
        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.accountId = account2;
        groupAccount2.group = group2;
        groupAccount2.joinDate = new Date();
//        đối tượng 3 :
        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.accountId = account4;
        groupAccount3.group = group3;
        groupAccount3.joinDate = new Date();
//        ----- Khởi tạo giá trị cho đối tượng  TypeQuestion-----
//        đối tượng 1 :
       TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = TypeName.ESSAY;

//        đối tượng 2 :
        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName = TypeName.ESSAY;
//        đối tượng 3 :
        TypeQuestion typeQuestion3 = new TypeQuestion();
        typeQuestion3.typeId = 3;
        typeQuestion3.typeName = TypeName.MULTIPLE_CHOICE;
//        ----- Khỏi tạo giá trị cho đối tượng  CategoryQuestion-----
//        đối tượng 1 :
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = "java";

//        đối tượng 2 :
        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryId = 2;
        categoryQuestion2.categoryName = "SQL";
//        đối tượng 3 :
        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryId = 3;
        categoryQuestion3.categoryName = "Ruby";
//        ----- Khỏi tạo giá trị cho đối tượng  Question-----
//        đối tượng 1 :
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "cau hoi 1";
        question1.category = categoryQuestion1;
        question1.typeId = typeQuestion1;
        question1.creatorId = account1;
        question1.createDate = new Date();

//        đối tượng 2 :
        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "cau hoi 2";
        question2.category = categoryQuestion2;
        question2.typeId = typeQuestion2;
        question2.creatorId = account2;
        question2.createDate = new Date();
//        đối tượng 3 :
        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "cau hoi 3";
        question3.category = categoryQuestion3;
        question3.typeId = typeQuestion3;
        question3.creatorId = account4;
        question3.createDate = new Date();
//        ----- Khỏi tạo giá trị cho đối tượng  Answer-----
//        đối tượng 1 :
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.content = "cau tra loi 1";
        answer1.question = question1;
        answer1.isCorrect = true;

//        đối tượng 2 :
        Answer answer2 = new Answer();
        answer2.answerId = 2;
        answer2.content = "cau tra loi 2";
        answer2.question = question2;
        answer2.isCorrect = true;
//        đối tượng 3 :
        Answer answer3 = new Answer();
        answer3.answerId = 3;
        answer3.content = "cau tra loi 3";
        answer3.question = question3;
        answer3.isCorrect = false;
        //        ----- Khỏi tạo giá trị cho đối tượng  Exam-----
//        đối tượng 1 :
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "code01";
        exam1.tile = "de thi 1";
        exam1.creatorId = account1;
        exam1.duration = 90;
        exam1.createDate = new Date();

//        đối tượng 2 :
        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = "code02";
        exam2.tile = "de thi 2";
        exam2.creatorId = account2;
        exam2.duration = 60;
        exam2.createDate = new Date();
//        đối tượng 3 :
        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = "code03";
        exam3.tile = "de thi 3";
        exam3.creatorId = account4;
        exam3.duration = 30;
        exam3.createDate = new Date();

//        ----- Khỏi tạo giá trị cho đối tượng  ExamQuestion---
//        đối tượng 1 :
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.exam = exam1;
        examQuestion1.question = question1;
//        đối tượng 2 :
        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.exam = exam2;
        examQuestion2.question = question2;
//        đối tượng 3 :
        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.exam = exam3;
        examQuestion3.question = question3;


        Account[] accounts1 = {account1, account2};
        group1.accounts = accounts1;
        Group[] groups1 = {group1, group2, group3};
        account4.groups = groups1;
        account2.groups = new Group[]{group1, group2};


//    ------------------------ Assignemnt2----------------------

        System.out.println("------- Question 1 -------");
        if (account2.department == null) {
            System.out.println("nhân viên này chưa có phòng ban ");
        } else {
            System.out.println("Phòng ban của nhân viên này là " + account2.department.departmentName);
        }

        System.out.println(" ");
        System.out.println("------- Question 2 -------");
        if (account2.groups == null) {
            System.out.println("Nhân viên này chưa có group");
        } else if (account2.groups.length <= 2) {
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if (account2.groups.length == 3) {
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
        } else {
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        }
//toán tủ 3 ngôi hoặc ternary ;
        System.out.println(" ");
        System.out.println("------- Question 3 -------");
//        Cach1
        String dpt = account2.department == null ? "nhân viên này chưa có phòng ban" : "Phòng ban của nhân viên này là " + account2.department.departmentName;
        System.out.println(dpt);
//        Cach 2
        System.out.println(account2.department == null ? "nhân viên này chưa có phòng ban" : "Phòng ban của nhân viên này là " + account2.department.departmentName);

        System.out.println(" ");
        System.out.println("------- Question 4 -------");
        System.out.println(account1.position.positionName == PositionName.DEV ? "Đây là Developer" : "Người này không phải là Developer");
//---------Question 5:---------
        System.out.println(" ");
        System.out.println("------- Question 5 -------");
        switch (group1.accounts.length) {
            case 1:
                System.out.println("Nhóm có một thành viên");
                break;
            case 2:
                System.out.println("Nhóm có hai thành viên");
                break;
            case 3:
                System.out.println("Nhóm có ba thành viên");
                break;
            case 4:
                System.out.println("Nhóm có nhiều thành viên");
                break;
        }
//           ----------Question 6-----------
        System.out.println(" ");
        System.out.println("------- Question 6 -------");
        int grouptlength = account2.groups.length;
        switch (grouptlength) {
            case 1:
                System.out.println("Nhân viên này chưa có group");
                break;
            case 2:
                System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
                break;
            case 3:
                System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
                break;
            case 4:
                System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
                break;
        }
////-----------Question 7: -------------------
        System.out.println(" ");
        System.out.println("------- Question 7 -------");
        switch (account1.position.positionName.toString()) {
            case "DEV":
                System.out.println("Đây là Developer");
                break;
            default:
                System.out.println("Người này không phải là Developer");
                break;

        }

//        FOREACH
////-----------Question 8: -------------------
        System.out.println(" ");
        System.out.println("------- Question 8 -------");
        Account[] accounts = {account1, account2, account3, account4};
        for (Account account : accounts) {
            System.out.println(account.email);
            System.out.println(account.fullName);
            System.out.println(account.department.departmentName);
        }
////-----------Question 9: -------------------
        System.out.println(" ");
        System.out.println("------- Question 9 -------");
        Department[] departments = {department1, department2, department3};
        for (Department department : departments) {
            System.out.println(department.departmentId);
            System.out.println(department.departmentName);
        }

////-----------Question 10: -------------------
        System.out.println(" ");
        System.out.println("------- Question 10 -------");
        for (int i = 0; i < accounts.length; i++) {
            System.out.println("Email: " + accounts[i].email);
            System.out.println("Full name: " + accounts[i].fullName);
            System.out.println("Phòng ban: " + accounts[i].department.departmentName);
            System.out.println(" ");
        }
////-----------Question 11: -------------------
        System.out.println(" ");
        System.out.println("------- Question 11 -------");
        for (int i = 0; i < departments.length; i++) {
            System.out.println("Id: " + departments[i].departmentId);
            System.out.println("Name: " + departments[i].departmentName);
        }
////-----------Question 12: -------------------
        System.out.println(" ");
        System.out.println("------- Question 12 -------");
        for (int i = 0; i < departments.length; i++) {
            if (i < 2) {
                System.out.println("Id: " + departments[i].departmentId);
                System.out.println("Name: " + departments[i].departmentName);
            }
        }
////-----------Question 13: -------------------
        System.out.println(" ");
        System.out.println("------- Question 13 -------");
        for (int i = 0; i < accounts.length; i++) {
            if (i != 1) {
                System.out.println("Email: " + accounts[i].email);
                System.out.println("Full name: " + accounts[i].fullName);
                System.out.println("Phòng ban: " + accounts[i].department.departmentName);
                System.out.println(" ");
            }
        }
////-----------Question 14: -------------------
        System.out.println(" ");
        System.out.println("------- Question 14 -------");
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId < 4) {
                System.out.println("Email: " + accounts[i].email);
                System.out.println("Full name: " + accounts[i].fullName);
                System.out.println("Phòng ban: " + accounts[i].department.departmentName);
                System.out.println(" ");
            }
        }
////-----------Question 15: -------------------
        System.out.println(" ");
        System.out.println("------- Question 15 -------");
        for (int i = 0; i <= 20; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
////-----------Question 16:Question10 -------------------
        System.out.println(" ");
        System.out.println("------- Question 16 - Question10-------");
        int i=0;
        while (i<accounts.length){
            System.out.println("Thông tin account thứ "+ (i + 1) +" là:");
            System.out.println("Email: "+accounts[i].fullName);
            System.out.println("Full name: "+accounts[i].email);
            System.out.println("Phòng ban: "+accounts[i].department.departmentName);
            i++;
        }
////-----------Question 16:Question11 -------------------
        Department[] departments1 = {department1,department2,department3};
        System.out.println(" ");
        System.out.println("------- Question 16 - Question11-------");
        int j=0;
        while (j<departments.length){
            System.out.println("Thông tin department thứ "+ (j + 1) +" là:");
                System.out.println("Id: "+departments1[j].departmentId);
                System.out.println("Name: "+departments1[j].departmentName);
            j++;
        }
//-----------Question 16:Question12 -------------------
        System.out.println(" ");
        System.out.println("------- Question 16 Question12-------");
        int a = 0;
        while (a < departments.length) {
            if (a < 2) {
                System.out.println("Thông tin department thứ "+ (a + 1) +" là:");
                System.out.println("Id: "+accounts[a].accountId);
                System.out.println("Name: "+accounts[a].fullName);
            }
            a++;
        }
//-----------Question 16:Question13 -------------------
        System.out.println(" ");
        System.out.println("------- Question 16 Question13-------");
        int b=0;
        while (b<accounts.length){
            if (b != 1){
                System.out.println(accounts[b].email);
                System.out.println(accounts[b].accountId);
                System.out.println(accounts[b].department);
                System.out.println(accounts[b].fullName);
            }
            b++;
        }
//-----------Question 16:Question14 -------------------
        System.out.println(" ");
        System.out.println("------- Question 16 Question14-------");
        int c = 0;
        while (c<accounts.length){
            if (c < 4){
                System.out.println(accounts[c].accountId);
                System.out.println(accounts[c].email);
                System.out.println(accounts[c].department);
                System.out.println(accounts[c].fullName);
            }
            c++;
        }
//-----------Question 16:Question15 -------------------
        System.out.println(" ");
        System.out.println("------- Question 16 Question15-------");
        int d = 0 ;
        while (d<=20 ){
            if (d%2==0){
                System.out.println(d);
            }
            d++;
        }
//-----------Question 17:Question10 -------------------
        System.out.println(" ");
        System.out.println("------- Question 17 Question10-------");
        int e=0;
        do {
            System.out.println("Thông tin department thứ "+ (e + 1) +" là:");
            System.out.println(accounts[e].fullName);
            System.out.println(accounts[e].department.departmentName);
            e++;
        }while (e < accounts.length);

//-----------Question 17:Question11 -------------------
        System.out.println(" ");
        System.out.println("------- Question 17 Question11------");
        int f=0;
        do {
            System.out.println("Thông tin department thứ "+ (f + 1) +" là:");
            System.out.println(departments[f].departmentId);
            System.out.println(departments[f].departmentName);
            f++;
        }while (f<departments.length);

//-----------Question 17:Question12 -------------------
        question17_2(departments);
//-----------Question 17:Question13 -------------------
//        question17(accounts);
//-----------Question 17:Question14 -------------------
//        question17_4(accounts);
//-----------Question 17:Question15 -------------------
        question17_5();

    }
    public static void question17_2 (Department[] departments){
        System.out.println(" ");
        System.out.println("------- Question 17 Question12------");
        int g = 0 ;
        do {
            if (g < 2){
                System.out.println("Thông tin department thứ "+ (g + 1) +" là:");
                System.out.println(departments[g].departmentId);
                System.out.println(departments[g].departmentName);
            }
            g++;
        }while (g<departments.length);
    }

    public static void question17_3 (Account[] accounts){
        System.out.println(" ");
        System.out.println("------- Question 17 Question13------");
        int m = 0;
        do {
            if (2 != accounts[m].accountId) {
                System.out.println(accounts[m].email);
                System.out.println(accounts[m].accountId);
                System.out.println(accounts[m].department.departmentName);
                System.out.println(accounts[m].fullName);
            }
            m++;
        }while (m < accounts.length);
    }
    public static void question17_4(Account[] accounts) {
        System.out.println(" ");
        System.out.println("------- Question 17 Question14------");
        int i = 0;
        do {
            if (i < 4) {
                System.out.println(accounts[i].accountId);
                System.out.println(accounts[i].email);
                System.out.println(accounts[i].department);
                System.out.println(accounts[i].fullName);
            }
            i++;
        } while (i< accounts.length);
    }
    public static void question17_5(){
        System.out.println(" ");
        System.out.println("------- Question 17_5-------");
        int i=0;
        do {
            if(i%2==0)
            System.out.println(i);
            i++;
        }while (i<=20);
    }


}

