package com.vti.frontend;

import java.util.Date;

public class Account {
    int accountId;
    String email;
    String username;
    String fullName;
    Department department;
    Position position;
    Date createDate;
    Group[] groups;
    float salary;
}
