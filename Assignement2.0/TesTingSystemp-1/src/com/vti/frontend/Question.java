package com.vti.frontend;

import java.util.Date;

public class Question {
    int questionId;
    String content;
    CategoryQuestion category;
    TypeQuestion typeId;
    Account creatorId;
    Date createDate;
}
