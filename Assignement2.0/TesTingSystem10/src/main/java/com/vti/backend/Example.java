package com.vti.backend;

import com.vti.Utils.JdbcUtils;
import com.vti.Utils.ScannerUtils;
import com.vti.entity.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Example {
    public void createAccount(String username, String emaill, String password) {
//         tạo 1 câi query --> tương ứng vói chức năng muốn sử dụng
        String sql = "INSERT INTO jdbc.Account (full_name, email , password) VALUE(? ,? ,?)";
//         kết nối tới database --> tạo một key làm việc
        Connection connection = JdbcUtils.GetConnection();
// không có biến truyền vào : statement
        try {

            //        tạo statement tương ứng với câu query
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, emaill);
            preparedStatement.setString(3, password);
//        execute câu query --> lấy kết quả (result)
            int resultSet = preparedStatement.executeUpdate();
//        kiểm tra sự thành công và thông tin
            if (resultSet == 0) {
//                System.out.println("kết nối thất bại");
            } else {
                System.out.println("kết nối thành công");
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Account> getAllAccount() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM jdbc.Account";
//        kết nối tới DB --> tạo một phiên làm việc
        Connection connection = JdbcUtils.GetConnection();
        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
//                lấy giá trị từng hàng gán vào đối tượng account tương ứng
                Account account = new Account();
                int accountID = resultSet.getInt("account_id");
                account.setAccountId(accountID);

                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));
                accounts.add(account);
            }
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return accounts;
    }

    public void updateAccount(String password, int accountId) {
        String sql = "UPDATE jdbc.Account a SET a.password = ? WHERE a.account_id = ?";
//        tạo một kết nối tới DB
        Connection connection = JdbcUtils.GetConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, password);
            preparedStatement.setInt(2, accountId);
//            excute : chạy câu querry trong sql
            preparedStatement.executeUpdate();
            System.out.println("update thành công");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public  void  UppdateAccount(int id , String oldpassword , String newpassword){
        String sql = "UPDATE jdbc.Account Set password = ? WHERE account_id = ? and password = ?";
        try {
            Connection connection = JdbcUtils.GetConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1 , newpassword);
            preparedStatement.setInt(2,id);
            preparedStatement.setString(3,oldpassword);

            int rs = preparedStatement.executeUpdate();
            if (rs == 0){
                System.out.println("thay đỏi password thành công");
            }else {
                System.out.println("thay đổi psssword không thành công");
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void deleteAccount(int accountId) {
        String sql = "DELETE FROM jdbc.Account a WHERE a.account_id = ? ";
        Connection connection = JdbcUtils.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, accountId);
            preparedStatement.executeUpdate();
            System.out.println("Delete thành công");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
//     chữa bài tập
    public void delleteAccount(int id){
        String sql = "DELETE FROM  jdbc.Account   WHERE account_id = ? ";
        Connection connection = JdbcUtils.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1 , id);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0){
                System.out.println("xóa thất bại");
            }else {
                System.out.println("xóa thành công");
            }
            connection.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public List<Account> finbByEmail(String email) {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM jdbc.Account WHERE email LIKE ?";
        Connection connection = JdbcUtils.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, "%" + email + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                account.setAccountId(resultSet.getInt("account_id"));
                account.setEmail(resultSet.getString("email"));
                account.setFullName(resultSet.getString("full_name"));
                account.setPassword(resultSet.getString("password"));
                accounts.add(account);
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return accounts;
    }


    public boolean loginAccount(String fullName , String password){
        String sql = "SELECT * FROM jdbc.Account WHERE full_name = ? and password = ?";
        Connection connection = JdbcUtils.GetConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,fullName);
            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
           return resultSet.next() ;

        }catch (SQLException e){
            System.out.println(e.getMessage());
            return false;
        }
        }
    }

