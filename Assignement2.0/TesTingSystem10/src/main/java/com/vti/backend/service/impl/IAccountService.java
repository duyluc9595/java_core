package com.vti.backend.service.impl;

import com.vti.entity.Account;

import java.util.List;

public interface IAccountService {
    void createAccount(String username, String emaill, String password);

    void updateAccount(int id , String oldPassword , String newPassword);

    void deleteAccount(int id);

    List<Account> FinByEmail(String key);

    List<Account> getAccount();

    boolean login(String email , String password);

}
