package com.vti.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString

public class Account {
    private int accountId;
    private String email;
    private String username;
    private String fullName;
    private Date createDate;
    private String Password;


}