package com.vti.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Manager extends User {
    private int ExpInYear;
    private int ProjectId;
}
