package com.vti.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {
    protected int id;
    protected String fullName;
    protected String email;
    protected String password;
    protected Role role;
    protected Employee employee;
    protected Manager manager;
}
