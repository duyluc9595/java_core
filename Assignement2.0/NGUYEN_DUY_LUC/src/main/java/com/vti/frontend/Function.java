package com.vti.frontend;

import com.vti.backend.controller.UserController;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class Function {
    UserController userController = new UserController();
    public void findAllUserByProjectIdEmployee(){
        System.out.println("mời bạn nhập vào projectId");
        int projectId = ScannerUtils.inputNumber();
        List<User> userList = userController.findAllUserByProjectIdEmployee(projectId);
        System.out.println(userList);
        String leftAlignFormat = "| %-3s| %-18s | %-15s | %-15s | %-15s |%n";
        System.out.format("+----+--------------------+------------------+------------------+-----------------+%n");
        System.out.format("| id |     fullName       |       email      |     proskill     |        role     |%n");
        System.out.format("+----+--------------------+------------------+------------------+-----------------+%n");
        for (User user : userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getFullName(), user.getEmail(), user.getEmployee().getProSkill(), user.getRole().name());
        }
        System.out.format("+----+--------------------+------------------+------------------+-----------------+%n");
    }

    public void findAllUserByProjectIdManager(){
        System.out.println("mời bạn nhập vào projectId");
        int projectId = ScannerUtils.inputNumber();
        List<User> userList = userController.findAllUserByProjectIdManager(projectId);
        System.out.println(userList);
        String leftAlignFormat = "| %-3s| %-18s | %-15s | %-15s | %-15s |%n";
        System.out.format("+----+--------------------+------------------+------------------+-----------------+%n");
        System.out.format("| id |     fullName       |       email      |     ExpInYear    |        role     |%n");
        System.out.format("+----+--------------------+------------------+------------------+-----------------+%n");
        for (User user : userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getFullName(), user.getEmail(), user.getManager().getExpInYear(), user.getRole().name());
        }
        System.out.format("+----+--------------------+------------------+------------------+-----------------+%n");
    }

    public void login() {
//        System.out.println("mời bạn nhập vào email");
        String email = ScannerUtils.inputEmail();

//        System.out.println("mòi bạn nhập vào password");
        String password = ScannerUtils.inputPassword();
        User user = userController.login(email, password);

        while (user == null) {
            System.err.println("user hoặc mặt khẩu không chính xác , mời bạn nhập lại");
            System.out.println("mời bạn nhập vào email");
            email = ScannerUtils.inputEmail();
            System.out.println("mời bạn nhập vào password");
            password = ScannerUtils.inputPassword();

            user = userController.login(email, password);
        }
        if (Role.MANAGER == user.getRole()) {
            System.out.println("Đăng nhập thành công");
        } else {
            System.out.println("bạn không có quyền đăng nhập");
        }
    }
}
