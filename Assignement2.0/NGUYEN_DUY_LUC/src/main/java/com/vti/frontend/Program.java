package com.vti.frontend;

import com.vti.utils.ScannerUtils;

public class Program {
    Function function = new Function();

    public static void main(String[] args) {
        menuUser();
    }

    public static void menuUser() {
        Function function = new Function();
        System.out.println("....................................................");
        System.out.println("Mời bạn chọn chức năng :");
        System.out.println("1 : tìm kiếm danh sách useremployee theo project id  )");
        System.out.println("2 : tìm kiếm danh sách usermanager theo project id )");
        System.out.println("3 : login");
        System.out.println("4 : Thoát chương trình");
        int chose = ScannerUtils.inputNumber(1, 4);
        switch (chose) {
            case 1:
                function.findAllUserByProjectIdEmployee();
                break;
            case 2:
                function.findAllUserByProjectIdManager();
                break;
            case 3:
                function.login();
                break;
            case 4:
                return;
        }
        if (chose == 4) {
            System.out.println("Bạn đã log Out");
        }
    }
}
