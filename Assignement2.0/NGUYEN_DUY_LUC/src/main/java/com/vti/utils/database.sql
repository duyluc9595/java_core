DROP DATABASE IF EXISTS bai_thi;
CREATE DATABASE bai_thi;
USE bai_thi;

CREATE TABLE Manager(
                        id int AUTO_INCREMENT PRIMARY KEY,
                        ExpInYear int ,
                        ProjectId int
);

CREATE TABLE Employee(
                         id int AUTO_INCREMENT PRIMARY KEY,
                         ProjectId int ,
                         ProSkill varchar(50)
);

CREATE TABLE `User`(
                       id int ,
                       full_name VARCHAR(50) ,
                       email VARCHAR(50) ,
                       `password` VARCHAR(50) ,
                       `role` ENUM ('MANAGER','EMPLOYEE'),
                       Manager_id int ,
                       Employee_id int,
                       FOREIGN KEY  (Manager_id) REFERENCES Manager(id),
                       FOREIGN KEY   (Employee_id) REFERENCES Employee(id)
);