package com.vti.backend.repository;

import com.vti.entity.Employee;
import com.vti.entity.Manager;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    public List<User> findAllUserByProjectIdEmployee(int projectId) {
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM `User` u join Employee e on u.Employee_id = e.id WHERE e.ProjectId = ? AND u.`role`=  'EMPLOYEE'";
        Connection connection = JdbcUtil.GetConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, projectId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                Employee employee = new Employee();
                user.setId(resultSet.getInt("id"));
                user.setFullName(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                employee.setProSkill(resultSet.getString("ProSkill"));
                user.setEmployee(employee);
                user.setRole(Role.valueOf(resultSet.getString("Role")));
                userList.add(user);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            JdbcUtil.closeConnection();
        }
        return userList;
    }

    public List<User> findAllUserByProjectIdManager(int projectId) {
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM `User` u join Manager m on u.Manager_id = m.id WHERE m.ProjectId = ? AND u.`role`='MANAGER'";
        Connection connection = JdbcUtil.GetConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, projectId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                Manager manager = new Manager();
                user.setId(resultSet.getInt("id"));
                user.setFullName(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                manager.setExpInYear(resultSet.getInt("ExpInYear"));
                user.setManager(manager);
                user.setRole(Role.valueOf(resultSet.getString("Role")));
                userList.add(user);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            JdbcUtil.closeConnection();
        }
        return userList;
    }

    public User login(String email, String password) {
        String sql = "SELECT * FROM `User` WHERE email = ? AND password = ? ";
        Connection connection = JdbcUtil.GetConnection();
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setFullName(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(Role.valueOf(resultSet.getString("Role")));
                return user;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            JdbcUtil.closeConnection();
            return user;
        }
    }
}
