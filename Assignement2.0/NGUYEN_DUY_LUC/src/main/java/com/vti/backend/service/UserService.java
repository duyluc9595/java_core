package com.vti.backend.service;

import com.vti.backend.repository.UserRepository;
import com.vti.entity.User;

import java.util.List;

public class UserService implements IUserService{
    UserRepository userRepository = new UserRepository();

    @Override
    public List<User> findAllUserByProjectIdEmployee(int projectId) {
        return userRepository.findAllUserByProjectIdEmployee(projectId);
    }

    @Override
    public List<User> findAllUserByProjectIdManager(int projectId) {
        return userRepository.findAllUserByProjectIdManager(projectId);
    }

    @Override
    public User login(String email, String password) {
        return userRepository.login(email,password );
    }
}
