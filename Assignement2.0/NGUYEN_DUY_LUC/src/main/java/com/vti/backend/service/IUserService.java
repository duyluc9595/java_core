package com.vti.backend.service;

import com.vti.entity.User;

import java.util.List;

public interface IUserService {

    public List<User> findAllUserByProjectIdEmployee(int projectId);

    public List<User> findAllUserByProjectIdManager(int projectId);

    User login(String email, String password);
}
