package com.vti.backend.controller;

import com.vti.backend.service.UserService;
import com.vti.entity.User;

import java.util.List;

public class UserController {
    UserService userService = new UserService();

    public List<User> findAllUserByProjectIdEmployee(int projectId) {
        return userService.findAllUserByProjectIdEmployee(projectId);
    }


    public List<User> findAllUserByProjectIdManager(int projectId) {
        return userService.findAllUserByProjectIdManager(projectId);
    }

    public User login(String email, String password) {
        return userService.login(email,password );
    }
}
