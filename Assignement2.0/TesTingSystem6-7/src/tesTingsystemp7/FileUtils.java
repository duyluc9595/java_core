package tesTingsystemp7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileUtils {
public static boolean checkFileExists(String pathFile){
    File fite = new File(pathFile);
    if (fite.exists()){
        System.out.println("đường dẫn có tồn tại");
        return true;
    }else {
        System.out.println("đường dẫn không tồn tại ");
        return false;
    }
}
public static void createFile(String fileName){
    String pathFile = "data/" + fileName;
    File file = new File(pathFile);
    try {
        if (file.createNewFile()){
            System.out.println("tạo file thành công");
        }else {
            System.out.println("tạo file không thành công");
        }
    }catch (IOException e){
        System.out.println(e.getMessage());
    }
    }

    public static void deleteFile(String pathFile){
    File file = new File(pathFile);
    if (file.exists()){
        System.out.println(file.delete() ? "xóa thành công" : "xóa thất bại");
    }else {
        System.out.println("file không tồn tại");
    }
    }

    public static void getAllFile(String foderpath){
    File file = new File(foderpath);
    if (file.isDirectory()){
        String[] fileList = file.list();
        for (String fileName : file.list()){
            System.out.println(fileName);
        }
    }else {
        System.out.println("đường dẫn không hợp lệ");
    }
    }

    public static void readFile(String pathFile) throws IOException {
    if (checkFileExists(pathFile)){
        try {
            FileInputStream fileInputStream = new FileInputStream(pathFile);
            byte[] b = new byte[1824];
            int length = fileInputStream.read(b);
            while (length > -1){
                String content = new String(b , 0,length);
                System.out.println(content);
                length = fileInputStream.read(b);
            }
            fileInputStream.close();
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());

        }
    }
    }
}
