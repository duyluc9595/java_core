package frontend;

public class demoDebug {
    public static void main(String[] args) {
//                    vd1();
                    vd3();
    }
        public static void vd1() {
            for (int i = 0; i < 10; i++) {
                System.out.println("Số i là : " + i);
                if (i % 2 == 0) {
                    System.out.println("chỉ lấy số chẵn: " + i);
                    continue;
                }
                System.out.println("Số lẻ thì xuống đây " + i);
                if (i == 7) {
                    System.out.println("i = 7 thì dừng vòng lặp hiện tại");
                    break;
                }
            }
        }
    public static void vd3() {
        System.out.println(6);
        System.out.println(7);
        System.out.println(8);
        System.out.println(1);
//         chỗ này khả năng bị lỗi
        try {
            System.out.println(2/1);
        }catch(Exception ex){
            System.out.println("không thể chia co 0");
        }
        System.out.println(2/8);
        System.out.println(3);
        System.out.println(4);
        System.out.println(5);
        System.out.println(6);
        System.out.println(7);
    }
    public static void vd4(){
        int[] arr = {1,2,3,4};
        try{
            System.out.println(arr[4]);
        }catch (IndexOutOfBoundsException exception) {
            System.out.println("có lỗi sảy ra");
        }
    }

}



